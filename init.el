;;;;;;;;;;;;;;;;;;;;
;; Smooth startup ;;
;;;;;;;;;;;;;;;;;;;;

;; Turn off mouse interface early in startup to avoid momentary display
;; (if (fboundp 'menu-bar-mode) (menu-bar-mode -1))
;; (if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
;; (if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))

;; No splash screen
(setq inhibit-startup-screen t)

;; No scratch message
(setq initial-scratch-message nil)

;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Package configuration ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'package)

(setq package-enable-at-startup nil)
(package-initialize)

(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))

(defun gimme-my-packages ()
  (interactive)

  (let ((required-packages '(; ace-isearch
                             ace-jump-mode

                             ; ack-and-a-half

                             browse-kill-ring

                             clojure-mode
                             clojure-snippets
                             ; clojurescript-mode ;; creo que clojure-mode ahora lo contiene
                             clj-refactor
                             cider
                             company

                             easy-kill
                             easy-kill-extras

                             expand-region

                             flycheck

                             hydra

                             helm
                             helm-swoop
                             helm-projectile

                             magit

                             multiple-cursors

                             neotree

                             paradox

                             paredit

                             popwin

                             perspective
                             projectile
                             persp-projectile

                             rainbow-delimiters

                             smart-mode-line
                             smart-mode-line-powerline-theme

                             smartparens

                             smooth-scrolling

                             undo-tree

                             window-number
                             )))

    (dolist (p required-packages)
    (when (not (package-installed-p p))
        (package-install p)))))

;;;;;;;;;;;;;;;;;
;; Convenience ;;
;;;;;;;;;;;;;;;;;

;; easy-kill
(global-set-key [remap kill-ring-save] 'easy-kill)
(global-set-key [remap mark-sexp] 'easy-mark)


;; expand-region
(global-set-key (kbd "C-.") 'er/expand-region)
(global-set-key (kbd "C-,") 'er/contract-region)

;; helm
(require 'helm)
(helm-mode t)

(setq helm-split-window-preferred-function 'ignore)

(setq helm-mode-fuzzy-match t)
(setq helm-swoop-split-direction 'split-window-horizontally)
(setq helm-swoop-speed-or-color nil)

(setq helm-grep-file-path-style 'relative)

(global-set-key (kbd "C-x C-f") 'helm-find-files)
(global-set-key (kbd "C-x b") 'helm-mini)
(global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key (kbd "C-S-l") 'helm-locate)
(global-set-key (kbd "C-S-g") 'helm-projectile-grep)
(global-set-key (kbd "C-S-h") 'helm-projectile)

;; paredit
(global-set-key (kbd "M-(") 'paredit-wrap-sexp)
(global-set-key (kbd "M-U") 'paredit-splice-sexp-killing-backward)

;; perspective
(require 'perspective)
(persp-mode)
(setq persp-show-modestring t)

(defun switch-to-last-persp ()
  "Switches back to the last perspective"
  (interactive)
  (persp-switch (persp-name persp-last)))

(define-key global-map (kbd "M-O") 'switch-to-last-persp)

(defun fvaresi/persp-kill (arg)
  (interactive "P")

  (if arg
      (persp-kill nil)
    (persp-kill (persp-name persp-curr))))

(define-key perspective-map (kbd "c") 'fvaresi/persp-kill)

;; popwin
(require 'popwin)

(popwin-mode t)

(push '(" *undo-tree*" :width 0.3 :position right) popwin:special-display-config)
(push '("*Helm Find Files*" :height 0.5) popwin:special-display-config)
(push '("*helm mini*" :height 0.5) popwin:special-display-config)
(push '("*helm grep*" :height 0.5) popwin:special-display-config)
(push '("*helm locate*" :height 0.5) popwin:special-display-config)
(push '("*helm M-x*" :height 0.5) popwin:special-display-config)
(push '("*helm projectile*" :height 0.25) popwin:special-display-config)
(push '("*helm etags*" :height 0.5) popwin:special-display-config)
(push '("*Ack-and-a-half*" :height 0.5 :stick t) popwin:special-display-config)
(push '("*vc-diff*" :height 0.5 :stick t) popwin:special-display-config)

;; projectile
(projectile-global-mode)
(setq projectile-svn-command projectile-generic-command)
(setq projectile-switch-project-action 'projectile-dired)

(require 'persp-projectile)
(projectile-persp-bridge helm-projectile)

;; rainbow delimiters
(require 'rainbow-delimiters)
(add-hook 'prog-mode-hook 'rainbow-delimiters-mode)

;; smartparens
(require 'smartparens-config)
(smartparens-global-strict-mode t)
(show-smartparens-global-mode t)
(sp-use-paredit-bindings)

;; smoth scrolling
(require 'smooth-scrolling)

;; window number
(require 'window-number)

(window-number-mode 1)
(window-number-meta-mode 1)

;;;;;;;;;;;;;
;; Clojure ;;
;;;;;;;;;;;;;

;; cider
(require 'cider)

(setq cider-repl-history-file "~/.emacs.d/cider-repl-history")

(setq nrepl-sync-request-timeout 40)

;; clj refactor
(require 'clj-refactor)

(defun my-clojure-mode-hook ()
  (eldoc-mode 1)

  (setq cider-repl-display-in-current-window t)

  (clj-refactor-mode 1)
  (yas-minor-mode 1)
  (cljr-add-keybindings-with-prefix "C-c C-m"))

(add-hook 'clojure-mode-hook #'my-clojure-mode-hook)

;;;;;;;;;;;;;;;;;;;;;
;; General Editing ;;
;;;;;;;;;;;;;;;;;;;;;

;; company
(global-company-mode)

;; browse-kill-ring
(require 'browse-kill-ring)
(browse-kill-ring-default-keybindings)

;; This enables replacing of text when selected
(delete-selection-mode t)

;; Split window horizontally for ediff.
(setq ediff-split-window-function 'split-window-horizontally)

;; Ignore case for file name completion.
(setq read-file-name-completion-ignore-case t)

;; Enable undo-tree-mode in all buffers.
(global-undo-tree-mode t)

;; Enable downcase/upcase of regions.
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)

;; Transpose bindings
(global-unset-key (kbd "M-t")) ;; which used to be transpose-words
(global-set-key (kbd "M-t l") 'transpose-lines)
(global-set-key (kbd "M-t s") 'transpose-sexps)
(global-set-key (kbd "M-t w") 'transpose-words)

;;;;;;;;;;;;;;;;;;;;
;; Editing Defuns ;;
;;;;;;;;;;;;;;;;;;;;

(defun fvaresi/add-empty-line-after ()
  "Add empty line after current one"
  (interactive)
  (end-of-line)
  (newline)
  (indent-for-tab-command))

(global-set-key (kbd "<C-return>") 'fvaresi/add-empty-line-after)

(defun fvaresi/add-empty-line-before ()
  "Add empty line before current one"
  (interactive)
  (beginning-of-line)
  (newline)
  (forward-line -1)
  (indent-for-tab-command))

(global-set-key (kbd "<C-S-return>") 'fvaresi/add-empty-line-before)

;; Join and expand lines
(defun fvaresi/join-line ()
  "Join lines"
  (interactive)
  (join-line -1))

(global-set-key (kbd "M-j") 'fvaresi/join-line)
(global-set-key (kbd "C-j") 'emmet-expand-line)

;; Duplicate selected region
(defun fvaresi/duplicate-region (&optional num start end)
  "Duplicates the region bounded by START and END NUM times.
      If no START and END is provided, the current region-beginning and
      region-end is used."
  (interactive "p")
  (save-excursion
    (let* ((start (or start (region-beginning)))
	   (end (or end (region-end)))
	   (region (buffer-substring start end)))
      (goto-char end)
      (dotimes (i num)
	(insert region)))))

(defun fvaresi/duplicate-current-line (&optional num)
  "Duplicate the current line NUM times."
  (interactive "p")
  (save-excursion
    (when (eq (point-at-eol) (point-max))
      (goto-char (point-max))
      (newline)
      (forward-char -1))
    (fvaresi/duplicate-region num (point-at-bol) (1+ (point-at-eol)))))

(defun fvaresi/duplicate-current-line-or-region (arg)
  "Duplicates the current line or region ARG times.
      If there's no region, the current line will be duplicated."
  (interactive "p")
  (if (region-active-p)
      (let ((beg (region-beginning))
	    (end (region-end)))
	(fvaresi/duplicate-region arg beg end)
	(one-shot-keybinding "d" (λ (fvaresi/duplicate-region 1 beg end))))
    (fvaresi/duplicate-current-line arg)
    (one-shot-keybinding "d" 'fvaresi/duplicate-current-line)))

(global-set-key (kbd "C-2") 'fvaresi/duplicate-current-line-or-region)

;; Window functions
(defun split-window-right-and-move-there-dammit ()
  (interactive)
  (split-window-right)
  (windmove-right))

(global-set-key (kbd "C-x 3") 'split-window-right-and-move-there-dammit)

(defun toggle-window-split ()
  (interactive)
  (if (= (count-windows) 2)
      (let* ((this-win-buffer (window-buffer))
	     (next-win-buffer (window-buffer (next-window)))
	     (this-win-edges (window-edges (selected-window)))
	     (next-win-edges (window-edges (next-window)))
	     (this-win-2nd (not (and (<= (car this-win-edges)
					 (car next-win-edges))
				     (<= (cadr this-win-edges)
					 (cadr next-win-edges)))))
	     (splitter
	      (if (= (car this-win-edges)
		     (car (window-edges (next-window))))
		  'split-window-horizontally
		'split-window-vertically)))
	(delete-other-windows)
	(let ((first-win (selected-window)))
	  (funcall splitter)
	  (if this-win-2nd (other-window 1))
	  (set-window-buffer (selected-window) this-win-buffer)
	  (set-window-buffer (next-window) next-win-buffer)
	  (select-window first-win)
	  (if this-win-2nd (other-window 1))))))

(global-set-key (kbd "M-T") 'toggle-window-split)

(defun rotate-windows ()
  "Rotate your windows"
  (interactive)
  (cond ((not (> (count-windows)1))
	 (message "You can't rotate a single window!"))
	(t
	 (setq i 1)
	 (setq numWindows (count-windows))
	 (while  (< i numWindows)
	   (let* (
		  (w1 (elt (window-list) i))
		  (w2 (elt (window-list) (+ (% i numWindows) 1)))

		  (b1 (window-buffer w1))
		  (b2 (window-buffer w2))

		  (s1 (window-start w1))
		  (s2 (window-start w2))
		  )
	     (set-window-buffer w1  b2)
	     (set-window-buffer w2 b1)
	     (set-window-start w1 s2)
	     (set-window-start w2 s1)
	     (setq i (1+ i)))))))

(global-set-key (kbd "M-R") 'rotate-windows)

(defun fvaresi/kill-buffer-and-window (arg)
  (interactive "P")
  (if (and arg
	   (> (count-windows) 1))
      (kill-buffer-and-window)
    (kill-buffer)))

(global-set-key (kbd "C-x k") 'fvaresi/kill-buffer-and-window)

;; Navigation bindings
(define-key global-map (kbd "C-S-n") (lambda () (interactive) (dotimes (i 5) (next-line))))
(define-key global-map (kbd "C-S-p") (lambda () (interactive) (dotimes (i 5) (previous-line))))

(global-set-key (kbd "M-o") 'mode-line-other-buffer)
(global-set-key (kbd "C-o") 'ace-jump-word-mode)

(global-set-key (kbd "C-o") 'ace-jump-word-mode)

;;;;;;;;;;;;;;;;;;;;;
;; Version Control ;;
;;;;;;;;;;;;;;;;;;;;;

(setq magit-use-overlays nil)
(setq magit-last-seen-setup-instructions "1.4.0")
(setq magit-push-always-verify nil)

(global-set-key (kbd "C-c m") 'magit-status)
